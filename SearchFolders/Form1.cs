﻿using System;
using System.Windows.Forms;

namespace SearchFolders
{
    public partial class Form1 : Form
    {
        private Timer timer = new Timer(); //таймер
        private SearchBackgrounded sb; //основной класс с логикой поиска

        public Form1()
        {
            InitializeComponent();

            sb = new SearchBackgrounded(tree, labelFile, labelTime, labelCount);

            timer.Interval = 10;
            timer.Tick += new EventHandler(timerTick); //подписываемся на события Tick
            timer.Start();
        }

        //обработчик события Таймера
        private void timerTick(object sender, EventArgs e)
        {
            sb.TreeUpdate();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var winRegistry = new WinRegistry(textBoxDir, textBoxName, textBoxText, checkBoxText);
            winRegistry.SettingsLoad();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            var winRegistry = new WinRegistry(textBoxDir, textBoxName, textBoxText, checkBoxText);
            winRegistry.SettingsSave();
        }


        // кнопка выбора директории
        private void buttonStartDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fBD = new FolderBrowserDialog();
            if (fBD.ShowDialog() == DialogResult.OK)
                textBoxDir.Text= fBD.SelectedPath;
        }

        // кнопка поиска
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            sb.StartSearch(textBoxDir.Text,textBoxText.Text,textBoxName.Text,checkBoxText.Checked);
        }

        // кнопка паузы
        private void buttonPause_Click(object sender, EventArgs e)
        {
            sb.SearchPause();
        }
    }
}
