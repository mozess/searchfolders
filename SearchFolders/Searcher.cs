﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SearchFolders
{
    /// <summary>
    /// класс поиска узлов древа
    /// </summary>
    class Searcher 
    {
        public delegate void MethodContainer(); //объявляем делегат
        public event Action<string> pathFound; //объявляем событие найденной строки
        public event MethodContainer SearchIsOver; //объявляем событие завершения поиска

        /// <summary>
        /// Метод поиска путей к файлам подходящим под параметры
        /// </summary>
        /// <param name="startDir">Начальная директория для поиска</param>
        /// <param name="nameFile">Шаблон имени файла</param>
        /// <param name="textFile">Текст содержащийся в файле</param>
        /// <param name="textFlag">Флаг "Искать по тексту"</param>
        public void SearchString(string startDir, string nameFile, string textFile, bool textFlag)
        {
            //Ищем строки пути
            var folderStack = new Stack<string>();// стэк для записи папок
            folderStack.Push(startDir);

            while (folderStack.Count > 0)
            {
                string direct = folderStack.Pop();

                try
                {
                    foreach (string d in Directory.GetDirectories(direct)) //добавляем все папки в стэк
                    {
                        try
                        {
                            folderStack.Push(d);
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                catch
                {
                    continue;
                }

                try
                {
                    foreach (string f in Directory.EnumerateFiles(direct, "*" + nameFile + "*", SearchOption.TopDirectoryOnly)) //ищем в каждой из папок файлы  
                    {
                        if (textFlag)
                        {
                            try
                            {
                                if (File.ReadAllText(f).IndexOf(textFile) >= 0)
                                {
                                    pathFound(f); //выдаем строку пути к файлу наружу
                                }
                            }
                            catch
                            {
                                continue;
                            }
                        }
                        else
                        {
                            try
                            {
                                pathFound(f);
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }
                }
                catch
                {
                    continue;
                }
            }
            SearchIsOver(); //посылаем событие завершения поиска
        }
    }
}
