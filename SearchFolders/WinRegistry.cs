﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;

namespace SearchFolders
{
    class WinRegistry
    {
        private TextBox tBDir;
        private TextBox tBName;
        private TextBox tBText;
        private CheckBox cBText;
        /// <summary>
        /// Класс загрузки и выгрузки параметров поиска 
        /// </summary>
        /// <param name="tBDir">Поле "Директория"</param>
        /// <param name="tBName">Поле "Имя файла"</param>
        /// <param name="tBText">Поле "Текст в файле"</param>
        /// <param name="cBText">Чек бокс "Искать по тексту"</param>
        public WinRegistry(TextBox tBDir, TextBox tBName, TextBox tBText, CheckBox cBText)
        {
            this.tBDir = tBDir;
            this.tBName = tBName;
            this.tBText = tBText;
            this.cBText = cBText;
        }
        /// <summary>
        /// Метод выгрузки параметров поиска из реестра
        /// </summary>
        public void SettingsLoad()
        {
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey SearchProKey = currentUserKey.OpenSubKey("Search_Program");
            if (SearchProKey != null)
            {
                tBDir.Text = SearchProKey.GetValue("directory").ToString();
                tBName.Text = SearchProKey.GetValue("name").ToString();
                tBText.Text = SearchProKey.GetValue("text").ToString();
                cBText.Checked = Convert.ToBoolean(SearchProKey.GetValue("searchByText"));
                SearchProKey.Close();
            }
        }

        /// <summary>
        /// Метод сохранения параметров поиска в реестр
        /// </summary>
        public void SettingsSave()
        {
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey SearchProKey = currentUserKey.CreateSubKey("Search_Program");
            SearchProKey.SetValue("directory", tBDir.Text);
            SearchProKey.SetValue("name", tBName.Text);
            SearchProKey.SetValue("text", tBText.Text);
            SearchProKey.SetValue("searchByText", cBText.Checked);
            SearchProKey.Close();
        }
    }
}
