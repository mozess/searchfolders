﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Collections.Concurrent;

namespace SearchFolders
{
    // класс объеденяющий всю логику работы с потоком и записи в очередь
    public class SearchBackgrounded 
    {
        static public string dirName { get; set; }
        static private System.Diagnostics.Stopwatch startTime;
        static private TreeView treeFolders; //дерево папок
        static private Label lFile; //обрабатываемый файл
        static private Label lTime; //время выполнения
        static private Label lCount; //Кол-во обработанных файлов
        static private ConcurrentQueue<string> pathQueue = new ConcurrentQueue<string>(); //очередь
        private Thread trd; //поток
        private static ManualResetEvent _stopper; //флаг паузы
        /// <summary>
        /// класс объеденяющий всю логику работы с потоком и записи в очередь
        /// </summary>
        /// <param name="treeFolders1">Дерево папок</param>
        /// <param name="lFile1">Обрабатываемый файл</param>
        /// <param name="lTime1">Время выполнения</param>
        /// <param name="lCount1">Количество найденных файлов</param>
        public SearchBackgrounded(TreeView treeFolders1, Label lFile1, Label lTime1, Label lCount1)
        {
            treeFolders = treeFolders1; //дерево папок
            lFile = lFile1; //обрабатываемый файл
            lTime= lTime1; //время выполнения
            lCount= lCount1; //Кол-во обработанных файлов
        }

        #region Обработчики событий
        /// <summary>
        /// метод обработки события при нахождении строки адреса
        /// </summary>
        /// <param name="s">Строка пути к файлу</param>
        public void QueueAdd(string s)
        {
            pathQueue.Enqueue(s);
        }

        /// <summary>
        /// метод обработки события окончания поиска
        /// </summary>
        public void SearchStop()
        {
            trd.Abort();
        }
        #endregion

        #region Метод реализации поиска
        /// <summary>
        /// Метод реализации поиска
        /// </summary>
        /// <param name="searchDir">Стартовая директория</param>
        /// <param name="searchText">Текст содержащийся в файле</param>
        /// <param name="searchName">Шаблон имени файла</param>
        /// <param name="textFlag">Флаг - искать по тексту</param>
        public void StartSearch(string searchDir, string searchText, string searchName, bool textFlag)
        {
            if (trd != null)
            {
                if (trd.ThreadState == ThreadState.Running) //если поток работает - то выходим
                    return;
                else if (trd.ThreadState == ThreadState.Suspended || trd.ThreadState == ThreadState.SuspendRequested) //если приостановлен - выходим
                    return;
                else if ((trd.ThreadState == ThreadState.Aborted || trd.ThreadState == ThreadState.AbortRequested || trd.ThreadState == ThreadState.Stopped || trd.ThreadState == ThreadState.StopRequested) & pathQueue.Count > 0) //если статусы завершенные и а очередь не пустая - выходим
                    return;
            }

            //ПРЕДВАРИТЕЛЬНАЯ ПРОВЕРКА ПОДАВАЕМЫХ ДАННЫХ
            if (searchDir == "")
            {
                MessageBox.Show("Выберите директорию для начала");
                return;
            }
            if (textFlag)
            {
                if (searchText == "")
                {
                    MessageBox.Show("Вы не указали текст для поиска");
                    return;
                }
                if (searchName != "")
                {
                    DialogResult result = MessageBox.Show(
                    "У вас так же заполнено поле Шаблон имени, осуществлять поиск только в файлах подпадающих под шаблон имени",
                    "Сообщение",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);

                    if (result == DialogResult.Yes)
                    { }
                    if (result == DialogResult.No)
                    {
                        searchName = "";
                    }
                }
            }
            else
            {
                if (searchName == "")
                {
                    DialogResult result = MessageBox.Show(
                    "Вы не указали текст для поиска, искать все файлы",
                    "Сообщение",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);

                    if (result == DialogResult.Yes)
                    { }
                    if (result == DialogResult.No)
                        return;
                }
            }

            dirName = searchDir;
            startTime = System.Diagnostics.Stopwatch.StartNew();

            //ОЧИСТКА ДАННЫХ НА ФОРМЕ
            lFile.Text = "0";
            lCount.Text = "0";
            lTime.Text = "00:00:00";
            treeFolders.Nodes.Clear();

            Searcher Search = new Searcher();
            //Подписались на событие
            Search.pathFound += QueueAdd;
            Search.SearchIsOver += SearchStop;

            // создаем и запускаем новый поток с нашим счетчиком
            trd = new Thread(() => Search.SearchString(searchDir, searchName, searchText, textFlag));
            _stopper = new ManualResetEvent(true);
            // trd.IsBackground = true;
            trd.Start();

            if (Path.GetFileName(searchDir) == "")
            {
                if (searchDir.Length == 3)
                    treeFolders.Nodes.Add(searchDir, searchDir);
                else if (searchDir.Length > 3 & searchDir[searchDir.Length - 1] == Path.DirectorySeparatorChar)
                {
                    searchDir = searchDir.Remove(searchDir.Length - 1);
                    treeFolders.Nodes.Add(searchDir, Path.GetFileName(searchDir));
                }
            }
            else
            {
                treeFolders.Nodes.Add(searchDir, Path.GetFileName(searchDir));
            }

        }
        #endregion

        #region Метод реализации паузы
        /// <summary>
        /// Метод реализации паузы
        /// </summary>
        public void SearchPause()
        {
            if (trd == null)
                return;

            if (trd.ThreadState == ThreadState.Running) //если поток работает - то приостанавливаем
                trd.Suspend();
            else if (trd.ThreadState == ThreadState.Suspended || trd.ThreadState == ThreadState.SuspendRequested) //если уже был приостановлен - выходим
                return;
            else if ((trd.ThreadState == ThreadState.Aborted || trd.ThreadState == ThreadState.AbortRequested || trd.ThreadState == ThreadState.Stopped || trd.ThreadState == ThreadState.StopRequested) & pathQueue.Count == 0) //если статусы завершенные и очередь пустая - выходим
                return;

            _stopper.Reset();
            startTime.Stop();

            DialogResult result = MessageBox.Show(
            "Прекратить выполнение поиска",
            "Сообщение",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Information,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);

            if (result == DialogResult.Yes)
            {
                //остановить поиск
                if (trd.ThreadState == ThreadState.Suspended)
                {
                    trd.Resume();
                    trd.Abort();
                }
                pathQueue = new ConcurrentQueue<string>();
            }
            if (result == DialogResult.No)
            {
                if (trd.ThreadState == ThreadState.Suspended)
                {
                    trd.Resume();
                }
                _stopper.Set();
                startTime.Start();
            }
        }
        #endregion

        #region Метод, заполняет: дерево, поле обрабатываемый файл, поле количество найденных файлов, поле время выполнения
        /// <summary>
        /// Метод, заполняет: дерево, поле обрабатываемый файл, поле количество найденных файлов, поле время выполнения
        /// </summary>
        public void TreeUpdate()
        {
            if (pathQueue.Count > 0)
            {
                //time += new TimeSpan(0, 0, 0, 0, 10);
                var resultTime = startTime.Elapsed;
                lTime.Text = $"{resultTime.Hours:00}:{resultTime.Minutes:00}:{resultTime.Seconds:00}"; //($"{time.Hours:00}:{time.Minutes:00}:{time.Seconds:00}");
            }
            if (pathQueue.TryDequeue(out string strFile) == true)
            {
                string searchDir = dirName;
                paintNode(strFile, searchDir);
                lFile.Text = strFile;
                lCount.Text = ((Convert.ToInt32(lCount.Text)) + 1).ToString();
            }
        }
        #endregion

        #region Метод отрисовки узлов в TreeView
        /// <summary>
        /// Метод отрисовки узлов в TreeView
        /// </summary>
        /// <param name="strFile">Полный путь к файлу</param>
        /// <param name="startDir">Стартовая директория поиска</param>
        static public void paintNode(string strFile, string startDir)
        {
            char[] sep = new char[] { Path.DirectorySeparatorChar };
            string[] filAr = strFile.Substring(startDir.Length).Split(sep, StringSplitOptions.RemoveEmptyEntries);
            string search = startDir;
            TreeNode Parent;

            //пробегаемся по частям пути
            foreach (var fa in filAr)
            {
                if (treeFolders.Nodes.Find(search + Path.DirectorySeparatorChar + fa, true).Length > 0) { search = search + Path.DirectorySeparatorChar + fa; }
                else
                {
                    Parent = treeFolders.Nodes.Find(search, true)[0];
                    Parent.Nodes.Add(search + Path.DirectorySeparatorChar + fa, fa);
                    search = search + Path.DirectorySeparatorChar + fa;
                }
            }
        }
        #endregion

    }
}
